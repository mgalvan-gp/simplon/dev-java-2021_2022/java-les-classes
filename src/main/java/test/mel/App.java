package test.mel;

import test.mel.oop.User;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        userClass();
    }

    public static void userClass() {
        // System.out.println("Zut ! J'avais " + monney + " Il me reste que " +
        // melissa.buy(2) + " !");

        User melissa = new User("Melissa", "Galvan", "test@mail.fr", 100);
        User jorge = new User("Jorge", "Bloup", "jbloup@mail.fr", 30);

        // System.out.println(melissa.toString());
        melissa.sleep();
        melissa.socialize(jorge);

        // System.out.println(jorge);
        // System.out.println(jorge.sleep());
        // jorge.sleep();
        // System.out.println(jorge.socialize(melissa));
    }
}
