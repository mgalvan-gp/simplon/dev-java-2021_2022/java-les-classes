package test.mel.oop;

import java.util.Random;

public class User {
    private String firstname;
    private String name;
    // private String email;
    // private float monney;
    private int energy;

    public User(String firstname, String name, String email, int energy) {
        this.firstname = firstname;
        this.name = name;
        // this.email = email;
        this.energy = energy;
    }

    public String toString() {
        return "\nThis is a person. Firstname: " + firstname + " Surname: " + name + ". " + energy;
    }

    public void sleep() {
        Random r = new Random();
        energy += r.nextInt(40 - (-20)) + (-20);
        if (energy > 100) {
            energy = 100;
        }
        System.out.println(energy);
    }

    public void socialize(User u) {
        energy -= 10;
        System.out.println("Hello " + u.firstname + " how are you ? " + u.energy + "\n");

        if (u.energy >= 70) {
            System.out.println(u.firstname + ": I'm fine ! " + u.energy + "\n" + firstname + ": " + energy);
        } else if (u.energy >= 40 && u.energy <= 70) {
            System.out.println(u.firstname + ": I'm ok ! " + u.energy + "\n" + firstname + ": " + energy);
        } else if (u.energy <= 30) {
            System.out.println(u.firstname + ": I can't even... " + u.energy + "\n" + firstname + ": " + energy);
        }
    }

}
